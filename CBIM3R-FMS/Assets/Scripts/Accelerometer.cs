﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public class Accelerometer : MonoBehaviour
{

    public TextMesh Status_Text_1;
    public TextMesh Status_Text_2;
    public TextMesh Status_Text_3;

	public float xangle = 1.0f;
	public float yangle = 1.0f;
	public float zangle = 1.0f;


	// 	private readonly Rect Gyrotext = new Rect(50, 100, 500, 200);

 
	public Rect Gyrotext = new Rect(50, 100, 500, 200);

    // Use this for initialization
    void Start()
    {
		
    }

    // Update is called once per frame
    void Update()
    {
        var xrot = Mathf.Atan2(Input.acceleration.z, Input.acceleration.y);
        var yzmag = Mathf.Sqrt(Mathf.Pow(Input.acceleration.y, 2) +
                               Mathf.Pow(Input.acceleration.z, 2));
        var zrot = Mathf.Atan2(Input.acceleration.x, yzmag);

        var xangle = xrot * (180 / Mathf.PI) + 180;
        var zangle = -zrot * (180 / Mathf.PI);


        transform.eulerAngles = new Vector3(xangle, 0, zangle);

  //      Status_Text_1.text = string.Format("{0:0.00},{1:0.00}", xangle, zangle);
  //      Status_Text_2.text = string.Format("{0:0.00},{1:0.00}", xangle, zangle);
  //      Status_Text_3.text = string.Format("{0:0.00},{1:0.00}", xangle, zangle);

		// Gyrotext = xangle.ToString ();
	

    }

	public void OnGUI()
	{
		GUI.color = Color.green;

	//	GUI.Label(Gyrotext, "Gyro angular values: " + xangle.ToString() +" : "+ 0.ToString() +" : " + zangle.ToString());
		GUI.Label(Gyrotext, xangle.ToString() + " , " + "0.0" + " , " + zangle.ToString());
		//GUI.Label(Gyrotext, xangle.ToString() + " , " + 0.ToString() + " , " + zangle.ToString());

	}
	 
		//GUI.Label(Gyrotext, xangle + " , " + 0 + " , " + zangle);
	//	GUI.Label(Gyrotext, "XXXXXXXXXXXXXXXXXXXX");
	 

}