using UnityEngine;

// Will currently only work with Unity 3.5 Public Beta // http://www.ikriz.nl/category/tech/unity3d
public class Webcam : MonoBehaviour
{
    private WebCamTexture webcamTexture;

    
    void Start()
    {
        webcamTexture = new WebCamTexture();
        GetComponent<Renderer>().material.mainTexture = webcamTexture;
        webcamTexture.Play();

   }

    void OnGUI()
    {
        if (webcamTexture.isPlaying)
        {
            if (GUILayout.Button("Pause"))
            {
                webcamTexture.Pause();
            }
            if (GUILayout.Button("Stop"))
            {
                webcamTexture.Stop();
            }
        }
        else
        {
            if (GUILayout.Button("Play"))
            {
                webcamTexture.Play();
            }
        }
    }




}