﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


 



public class IviGui : MonoBehaviour {
	public Canvas ivcUI;
	public GameObject CamPlane;
	public GameObject CamRigController; //drag the gameobject which have that script you want to disable, in the inspector.

	// Use this for initialization
	void Start () {
		ivcUI.enabled = false;
		CamPlane.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

		OVRPlayerController script;

		script = CamRigController.GetComponent<OVRPlayerController>();

		if (OVRInput.GetDown (OVRInput.Button.Four)) 
		{
			Debug.Log ("IVC GUI is activited !!");
			ivcUI.enabled = true;
			//CamPlane.SetActive(true);

			script.enabled = true;

		}

		if (OVRInput.GetDown (OVRInput.Button.Three)) 
		{
			Debug.Log ("IVC GUI is disactivited !!");
			ivcUI.enabled = false;
			//CamPlane.SetActive(false);
		}

		// hold GUI
			if (OVRInput.GetDown (OVRInput.Button.Two)) 
		{
 		//	ivcUI.transform.parent = null;
		//CamPlane.transform.parent = null;
		

			script.enabled = false;

		}



	}
}
