﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class FieldTaskMarkingSketching : MonoBehaviour {


	public GameObject mark;
	public Transform taskMarkPosition;

	public static float fXvalue;
	public static float fYvalue;
	public static float fZvalue;

	// new
	public static float fSkthX;
	public static float fSkthY;
	public static float fSkthZ;
	// 

	//public Transform RayCamera;
	//public Transform PointingMark;
	public GameObject SketchingLine;


	public void Start()
	{
		//

	}
	//void Update () {


	void Update () {
		
	}

	void OnMouseDown()
	{
		


		//Ray ray = Camera.main.ScreenPointToRay (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z)); 
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); 



		RaycastHit hit;


		if (Physics.Raycast (ray, out hit)) {



			//PointingMark.transform.position = new Vector3 (hit.point.x, hit.point.y, hit.point.z);
			//	if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)) 
			//{

			var clone =	Instantiate (mark, hit.point, Quaternion.identity);

			fXvalue = hit.point.x;
			fYvalue = hit.point.y;
			fZvalue = hit.point.z;
			Vector3	taskMarkPosition = new Vector3 (fXvalue, fYvalue, fZvalue);


			//	Debug.Log ("The Marked Task Position at: " + taskMarkPosition);


			this.GetComponent<NetworkView> ().RPC (Constants.cRPCRemoteFeildValuesToSend, Constants.cSendMessagesMode, fXvalue, fYvalue, fZvalue);


		}
	}

			//if (OVRInput.Get(OVRInput.RawButton.RHandTrigger)) {
		void OnMouseDrag()
		{
			

				SketchingLine.SetActive(true);

				// new
				fSkthX=	SketchingLine.transform.position.x;
				fSkthY=	SketchingLine.transform.position.y;
				fSkthZ=	SketchingLine.transform.position.z;
				// 


				Debug.Log ("Sketching in process!!!!!");

				// pass the values for the remote sketch

		this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteSketchFeildValuesToSend, Constants.cSendMessagesMode, fSkthX, fSkthY, fSkthZ); // new

}


	void OnMouseUp()
	{
		//	if (OVRInput.GetUp(OVRInput.RawButton.RHandTrigger)) {

				SketchingLine.SetActive(false);

				// SketchingLine = null; // new

				Debug.Log ("Sketching in stopped!!!!!");

			}
		



	[RPC]
	public void RPCRemoteFeildValuesToSend(float fXvalue, float fYvalue, float fZvalue)
	{
		Instantiate (mark, new Vector3(fXvalue,fYvalue,fZvalue), Quaternion.identity);

	}

	[RPC]
	public void RPCRemoteSketchFeildValuesToSend(float fSkthX, float fSkthY,float fSkthZ)  // new
	{

	}
}

