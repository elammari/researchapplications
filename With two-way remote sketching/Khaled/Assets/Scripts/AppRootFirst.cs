using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using System;
using UnityEngine.UI;

public class AppRootFirst : MonoBehaviour
{
    ///////////////////////////////////////////////////////////////////////////
    #region Variables

    // rects for rendering gui elements
    private readonly Rect cSendHelloRect = new Rect(20, 200, 70, 50);
	private readonly Rect cSendDataRect = new Rect(20, 260, 70, 50);
	private readonly Rect cValuesToSendRect = new Rect(20, 320, 70, 50);

    private readonly Rect cDebugMsgRect = new Rect(20, 420, 200, 100);

    // debug message and data
    private string mSendMessage = "No messages";
    private byte[] mDataToSend = new byte[] { 0x0b, 0x1f, 0x3c };

	private float mValuesToSend = 0.0f;

    // timer to create some delay for sending messages
    private float mWaitTimeUpdate = 0.0f;
    private const float cMaxWaitTimeUpdate = 0.1f;


    private readonly Rect accelXRect = new Rect(50, 50, 400, 100);
    // private string accelXRectmessage = "No values shown!!";
    public float AccelX = 1.0f;
    public float AccelY = 1.0f;
    public float AccelZ = 1.0f;

	public Transform UserLocation;


	public float ARPoseX = 1.0f;
	public float ARPoseY = 1.0f;
	public float ARPoseZ = 1.0f;

	public float ARPoseRotationX = 1.0f;
	public float ARPoseRotationY = 1.0f;
	public float ARPoseRotationZ = 1.0f;

	public Transform RemoteTaskTag;
	public GameObject RemoteTaskObj;
	//public Transform RemoteSketchTaskTag;
	//public GameObject RemoteSkketchTaskObj;



	public TrailRenderer lineObject;
	public float offset =1.0f;
	private TrailRenderer lineObjectInstance;
	public GameObject wall;
	public GameObject SketchingLine;

     
	public InputField ChatInputFieldAR;

	//private string valuesTxt;

    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Interface
    public void Start()
    {
        //
        InitNet();
    }

    public void Update()
    {







        //  AccelX = Input.gyro.rotationRateUnbiased.x;
        //  AccelY = Input.gyro.rotationRateUnbiased.y;
        //  AccelZ = Input.gyro.rotationRateUnbiased.z;

		AccelX = Input.acceleration.x;
		//AccelX = Input.gyro.rotationRateUnbiased.y;
        AccelY = Input.acceleration.y;
        AccelZ = Input.acceleration.z;


		ARPoseX = UserLocation.GetComponent<RectTransform> ().position.x;
		ARPoseY = UserLocation.GetComponent<RectTransform> ().position.y;
		ARPoseZ = UserLocation.GetComponent<RectTransform> ().position.z;


		ARPoseRotationX = UserLocation.GetComponent<RectTransform> ().rotation.x;
		ARPoseRotationY = UserLocation.GetComponent<RectTransform> ().rotation.y;
		ARPoseRotationZ = UserLocation.GetComponent<RectTransform> ().rotation.z;

        //   this.GetComponent<NetworkView>().RPC(Constants.cRPCSendGyroData, Constants.cSendMessagesMode, mGyroDataToSend);


      // KH  Debug.Log("The Gyro values are: " + AccelX + " , " + AccelY + " , " + AccelZ);

        //     gyr = "The Gyro values are:" + AccelX + " , " + AccelY + " , " + AccelZ + "&&&&&&&&&&&&&&&&&&&"+ mGyroDataToSend;


		//valuesTxt = (accelXRect, AccelX.ToString() + " , " + AccelY.ToString() + " , " + AccelZ.ToString());


    }

    public void OnGUI()
    {
        // increment wait time
        if (mWaitTimeUpdate < cMaxWaitTimeUpdate)
        {
            mWaitTimeUpdate += Time.deltaTime;
        }
        



	  //   Start of communication channel & buttons

        if (GUI.Button(cSendHelloRect, "Hello"))
        {
            if (mWaitTimeUpdate > cMaxWaitTimeUpdate)
            {
                //
            //  this.GetComponent<NetworkView>().RPC(Constants.cRPCSendMessage, Constants.cSendMessagesMode, "Hello");
				this.GetComponent<NetworkView>().RPC(Constants.cRPCSendMessage, Constants.cSendMessagesMode, AccelX.ToString()+ " , " + AccelY.ToString() + " , " + AccelZ.ToString());


                //
                mWaitTimeUpdate = 0.0f;

                //
                mSendMessage = "'Hello' message sent";
            }
        }





        if (GUI.Button(cSendDataRect, "Send data"))
        {
            if (mWaitTimeUpdate > cMaxWaitTimeUpdate)
            {
                //
                this.GetComponent<NetworkView>().RPC(Constants.cRPCSendData, Constants.cSendMessagesMode, mDataToSend);

                //
                mWaitTimeUpdate = 0.0f;

                //
                mSendMessage = "Data sent";
            }
        }


		if (GUI.Button(cValuesToSendRect, "Send Value"))
		{
			if (mWaitTimeUpdate > cMaxWaitTimeUpdate)
			{
				//
				//  this.GetComponent<NetworkView>().RPC(Constants.cRPCSendMessage, Constants.cSendMessagesMode, "Hello");
				this.GetComponent<NetworkView>().RPC(Constants.cRPCValuesToSend, Constants.cSendMessagesMode, AccelX, AccelY, AccelZ, ARPoseX, ARPoseY, ARPoseZ, ARPoseRotationX, ARPoseRotationY, ARPoseRotationZ);


				//
				mWaitTimeUpdate = 0.0f;

				//
				mSendMessage = "'Hello' message sent";
			}
		}


        GUI.Label(cDebugMsgRect, mSendMessage);




        GUI.Label(accelXRect, AccelX.ToString() + " , " + AccelY.ToString() + " , " + AccelZ.ToString());

		                   //   End of communication channel & buttons 
    }

    ///////////////////////////////////////////////////////////////////////////
    #region RPC functions

    [RPC]
    public void RPCSendMessage(string msg)
    {

    }

    [RPC]
    public void RPCSendData(byte[] data)
    {

    }

	[RPC]
	public void RPCValuesToSend(float vluX, float vluY , float vluZ, float ARPoseX, float ARPoseY , float ARPoseZ, float ARPoseRotationX, float ARPoseRotationY, float ARPoseRotationZ)
	{

	}


	[RPC]
	public void RPCRemoteValuesToSend(float Xvalue, float Yvalue, float Zvalue)  // new

	//public void RPCRemoteValuesToSend(float Xvalue, float Yvalue, float Zvalue)
	{

		Instantiate(RemoteTaskObj, new Vector3(Xvalue, Yvalue, Zvalue), Quaternion.identity);

		//SketchingLine.SetActive(true);
	//	SketchingLine.transform.position = new Vector3 (SkthX, SkthY, SkthZ);

		RemoteTaskTag.transform.Translate (new Vector3 (Xvalue, Yvalue, Zvalue));

	//	Debug.Log ("My remote values are ::::"+Xvalue+"---"+Yvalue+"---"+Zvalue);

	//	Vector3	RemoteTaskTag = new Vector3(Xvalue,Yvalue,Zvalue);
		//Instantiate(

		mSendMessage = "Facility Manager has added a task tag";


	}

	[RPC]
	public void RPCRemoteSketchValuesToSend(float SkthX, float SkthY,float SkthZ)  // new

	//public void RPCRemoteValuesToSend(float Xvalue, float Yvalue, float Zvalue)
	{

	//	Instantiate(RemoteTaskObj, new Vector3(Xvalue, Yvalue, Zvalue), Quaternion.identity);

		SketchingLine.SetActive(true);
		SketchingLine.transform.position = new Vector3 (SkthX, SkthY, SkthZ);

	//	RemoteTaskTag.transform.Translate (new Vector3 (Xvalue, Yvalue, Zvalue));

		//	Debug.Log ("My remote values are ::::"+Xvalue+"---"+Yvalue+"---"+Zvalue);

		//	Vector3	RemoteTaskTag = new Vector3(Xvalue,Yvalue,Zvalue);
		//Instantiate(

		mSendMessage = "Facility Manager has added a task tag";


	}


	/*

	[RPC]
	public void RPCRemoteSketchValuesToSend(float Xvalue, float Yvalue, float Zvalue)
	{
		//	transform.position += Vector3.right * 2;

		Instantiate(RemoteSkketchTaskObj, new Vector3(Xvalue, Yvalue, Zvalue), Quaternion.identity);
		RemoteSketchTaskTag.transform.Translate (new Vector3 (Xvalue, Yvalue, Zvalue));



	}

	*/



	/*
	[RPC]
	public void RPCRemoteLineToSend(Vector3 lineMarkPosition)
	{
		 

	//	lineObjectInstance = Instantiate(lineObject,new Vector3(XLine,YLine,ZLine)+new Vector3(XLine,YLine,ZLine).normalized*offset, Quaternion.identity) as TrailRenderer;


	//	lineObjectInstance.transform.position=new Vector3(XLine,YLine,ZLine)+new Vector3(XLine,YLine,ZLine).normalized*offset;

		lineObjectInstance = Instantiate(lineObject,lineMarkPosition+lineMarkPosition.normalized*offset, Quaternion.identity) as TrailRenderer;

		lineObjectInstance.transform.position=lineMarkPosition+lineMarkPosition.normalized*offset;


		mSendMessage = "Linnnnnnnnnnnnnnnnnnnnnnne = " + lineMarkPosition.x;

	}

  */


    #endregion
    ///////////////////////////////////////////////////////////////////////////

    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Implementation

    /// <summary>
    /// Initializes RPC server
    /// </summary>
    private void InitNet()
    {
     //  Network.InitializeServer(3, Constants.cServerPort, false);
		Network.Connect(Constants.cServerIp, Constants.cServerPort);

    }

    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Properties

    #endregion
    ///////////////////////////////////////////////////////////////////////////
}
