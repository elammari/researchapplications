using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using System;
using UnityEngine.UI;

public class AppRootSecond : MonoBehaviour
{
    ///////////////////////////////////////////////////////////////////////////
    #region Variables

    // rect for displaying of received message
    private readonly Rect cMsgRect = new Rect(20, 420, 200, 100);

    // received message
    private string mReceiveMessage = "No messages";

	public string msg;
	public double numberx;

	public Transform Cam;
	//public Transform Img;
	//public float speed = 10.0f;

	public InputField ChatInputField;
	public Transform RemoteTaskTag;
	public GameObject SketchingLine;
	public GameObject RemoteTaskObj;


    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Interface
    public void Start()
    {
        InitNet();
    }

    public void Update()
    {

    }

    public void OnGUI()
    {
        // just show received message
        GUI.Label(cMsgRect, mReceiveMessage);
    }

    #region RPC functions

    [RPC]
    public void RPCSendMessage(string msg)
    {
        mReceiveMessage = "Message received = " + msg;
		Debug.Log("The passed Gyro values are:" + msg);

    }


    [RPC]
    public void RPCSendData(byte[] data)
    {
        mReceiveMessage = "Data received. Data length = " + data.Length;
    }

	[RPC]
	public void RPCValuesToSend(float vluX, float vluY , float vluZ, float ARPoseX, float ARPoseY , float ARPoseZ, float ARPoseRotationX, float ARPoseRotationY, float ARPoseRotationZ) // Received value from the android AR app
	{
		mReceiveMessage = "Message received = " + vluX;
		Debug.Log("The passed Gyro values are:" + vluX +" , "+ vluY +" , "+ vluZ +" , "+ ARPoseX +" , "+ ARPoseY +" , "+ ARPoseZ+" , "+ ARPoseRotationX +" , "+ ARPoseRotationY +" , "+ ARPoseRotationZ);


	//	Debug.Log(Vector3.Distance(Cam.position,Img.position).ToString());
		// Re-rotate OR Re-position the IVC module camera based on the received float value (vlu) from AR app. 
	//	Cam.transform.Translate(vlu,0,Time.deltaTime);
	
		//Cam.transform.Rotate (vluX, vluY, vluZ);
		//Cam.transform.Translate(ARPoseX, ARPoseY, ARPoseZ);

		Cam.transform.position= new Vector3(ARPoseX,ARPoseY,ARPoseZ);
		//Cam.transform.Rotate (ARPoseRotationX, ARPoseRotationY, ARPoseRotationZ);
	//	Cam.transform.rotation= Quaternion.Euler(ARPoseRotationX,ARPoseRotationY,ARPoseRotationZ);

	//	Cam.transform.eulerAngles= new Vector3(ARPoseRotationX,ARPoseRotationY,ARPoseRotationZ);

		//Cam.transform.eulerAngles= new Vector3(0,0,0);



		//Cam.transform.Translate( ARPoseRotationX, ARPoseRotationY,ARPoseRotationZ, Camera.main.transform);

	}

	// passed values from the field worker
	[RPC]
	public void RPCRemoteFeildValuesToSend(float fXvalue, float fYvalue, float fZvalue)
	{
		Instantiate(RemoteTaskObj, new Vector3(fXvalue, fYvalue, fZvalue), Quaternion.identity);

		//SketchingLine.SetActive(true);
		//	SketchingLine.transform.position = new Vector3 (SkthX, SkthY, SkthZ);

		RemoteTaskTag.transform.Translate (new Vector3 (fXvalue, fYvalue, fZvalue));

		Debug.Log ("Marking at "+fXvalue+" , "+fYvalue+" , "+fZvalue);
	}

	[RPC]
	public void RPCRemoteSketchFeildValuesToSend(float fSkthX, float fSkthY,float fSkthZ)  // new
	{
		SketchingLine.SetActive(true);
		SketchingLine.transform.position = new Vector3 (fSkthX, fSkthY, fSkthZ);
		Debug.Log ("Sketching at  at "+fSkthX+" , "+fSkthY+" , "+fSkthZ);

	}


//	[RPC]
//	public void RPCRemoteValuesToSend(float CamPosX, float CamPosY, float CamPosZ ) // Received value from the android AR app
//	{
	//	mReceiveMessage = "Message received = " + vluX;
	//	Debug.Log("The passed Gyro values are:" + vluX +" , "+ vluY +" , "+ vluZ +" , "+ ARPoseX +" , "+ ARPoseY +" , "+ ARPoseZ+" , "+ ARPoseRotationX +" , "+ ARPoseRotationY +" , "+ ARPoseRotationZ);


	//	transform.position= new Vector3(CamPosX,CamPosY,CamPosZ);
	

//	}


    #endregion

    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Implementation

    private void InitNet()
    {
		      Network.InitializeServer(3, Constants.cServerPort, false);

     //  Network.Connect(Constants.cServerIp, Constants.cServerPort);
    }

    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region Properties

    #endregion
    ///////////////////////////////////////////////////////////////////////////
}
