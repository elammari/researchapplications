﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DistanceCalculater : MonoBehaviour {

	public Transform ARCamera;
	public Transform Imgtarget;
	public Text Distancetext;
	public float Distxt;


	public void OnGUI()
	{
		
		Distxt= Vector3.Distance(ARCamera.position,Imgtarget.position);
		Distancetext.text = "You are " + Distxt.ToString ("f1") + " far from the wall !";

	}
}
