﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DrawLine : MonoBehaviour 
{

	public TrailRenderer lineObject;
	public float offset =1.0f;
	private TrailRenderer lineObjectInstance;
//	public Toggle ResetToggle;

	public void Start()
	{
		//
	
	}

	void OnMouseDown()
	{
	// Cast a ray and find where the Bulding element (e.g., wall)
		//Ray ray = Camera.main.ViewportPointToRay (Input.mousePosition));
	//	Ray ray = Camera.main.ScreenPointToRay (new Vector3(0.5f,0.5f,0.0f)); 
		Ray ray = Camera.main.ScreenPointToRay (new Vector3(Input.mousePosition.x,Input.mousePosition.y,Input.mousePosition.z)); 

	RaycastHit info;


		if (Physics.Raycast(ray, out info)) 
		
		{

			// Instantiate line renderer object

			lineObjectInstance = Instantiate(lineObject,info.point+info.normal*offset, Quaternion.identity) as TrailRenderer;

		//	lineObjectInstance = Instantiate(lineObject,info.point*-offset,Quaternion.identity) as TrailRenderer;

		}
}

	void OnMouseDrag()
	{
		// Check to see if we have a line object instance
		if(lineObjectInstance)
		{

		//	Ray ray = Camera.main.ViewportPointToRay(Input.mousePosition));
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); 

			RaycastHit info;

			if (Physics.Raycast(ray, out info))
			
			{

				// move our instantiated line renderer
				lineObjectInstance.transform.position=info.point+info.normal*offset;
			}

		}
	}

	void OnMouseUp()
	{
		// Leave the rendrer where we left it
		lineObjectInstance = null;
	}

	//public void OnGUI(){

	//	if (ResetToggle.isOn) {

		//	Debug.Log ("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
	//		Destroy (GameObject.Find("SphereTrial(Clone)"));
	//		Destroy (GameObject.Find("LineObject(Clone)"));
 	//	}
			

	//}
}
		
			
