﻿#pragma strict
// This scripet is codded to position the virtual hatch at the ray hit point
	var speed = 1;
	var targetRadius = 1;
	private var target : Vector3;
	private var cam : Transform ;

	function Start () {
		//    rigidbody.freezeRotation = true;
		target = transform.position;
	}

	function FixedUpdate () {
		target.y = transform.position.y;
	 
			var hit : RaycastHit; 
		 
			cam    = Camera.main.transform;

		 var ray = new Ray(cam.position, cam.forward);

			if (Physics.Raycast (ray, hit)) {
				target = hit.point;         
			}
	   

		if (Vector3.Distance(transform.position, target) > targetRadius) {
			transform.LookAt(target); 
			transform.Translate(0,0,speed);                 
		}
	}