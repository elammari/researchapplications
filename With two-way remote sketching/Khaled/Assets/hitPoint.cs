﻿using UnityEngine;
using System.Collections;

public class hitPoint : MonoBehaviour {

	//public GameObject target;
	Vector2 targetPos;

	public GameObject pointPrefabRed;
	public GameObject pointPrefabBlue;

	//public GameObject canvas;

	public float gain = 1.0f;

	public void Activate (GameObject target) {

		targetPos = new Vector2(target.transform.position.x, target.transform.position.y + gain);

		if (target.gameObject.tag == "Player 1")
		{
			//pointPrefabRed = Instantiate(pointPrefabRed, targetPos, Quaternion.identity) as GameObject;
			GameObject newPointRed = Instantiate(pointPrefabRed, targetPos, Quaternion.identity) as GameObject;
			newPointRed.transform.parent = gameObject.transform;
		}
		else if (target.gameObject.tag == "Player 2")
		{
			pointPrefabBlue = Instantiate(pointPrefabBlue, targetPos, Quaternion.identity) as GameObject;
			pointPrefabBlue.transform.parent = gameObject.transform;
		}
	}

}