﻿using UnityEngine;
using System.Collections;

public class TeleportOVRCam215 : MonoBehaviour {

	public GameObject teleportingPoint;
	public GameObject OvrCam;

	// Use this for initialization
	void Start () {

	
		OvrCam.transform.position = teleportingPoint.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
