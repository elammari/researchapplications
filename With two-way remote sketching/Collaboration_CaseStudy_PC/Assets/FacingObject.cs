﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public class FacingObject : MonoBehaviour 
 {
	public Camera camera;


	public Text FacingObjecttext;
	public Transform ARCamera;
	public Transform Imgtarget;
	public Text Distancetext;
	public float Distxt;

	void OnGUI() {
		RaycastHit hit;
		Ray ray = camera.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (ray, out hit)) {
			Transform objectHit = hit.transform;

		//	Debug.Log ("The facing object is: " + objectHit.transform.name);
	

			Distxt= Vector3.Distance(ARCamera.position,Imgtarget.position);
			Distancetext.text = "You are " + Distxt.ToString ("f1") + " m far from the wall";
			FacingObjecttext.text = "Facing [ " + objectHit.transform.name+" ]";


		}


	}



	}
