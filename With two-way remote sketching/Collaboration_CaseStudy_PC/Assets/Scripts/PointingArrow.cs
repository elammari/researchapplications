﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class PointingArrow : MonoBehaviour {
	public float range;
	public LineRenderer beam;
	public Material beamMaterial;

	public GameObject target;

	public GameObject muzzle;

	private GameObject _target;



	void Start () {
		beam = GetComponent<LineRenderer>();
		beam.SetVertexCount(2);
	//	beam.renderer.GetComponent<Material>() = beamMaterial;
		beam.SetWidth(0.001f, 0.025f);
	}

	void Update () {
		_target = target;

		DisplayBeam();
	}

	public void DisplayBeam() {
		if(_target != null) {


	
			Camera camera = GetComponent<Camera>();
		//	Camera.main.ScreenToViewportPoint
		Vector3	 p=	camera.ViewportToWorldPoint(new Vector3(0.5f,0.0f,camera.nearClipPlane));
		//	Camera.main.WorldToScreenPoint (target.transform.position);

		//	Gizmos.DrawSphere (p, 0.1f);
			beam.enabled = true;
			beam.SetPosition(0, p);
			beam.SetPosition(1, _target.transform.position);

		}
	}
}
	

