﻿using UnityEngine;
using System.Collections;
using System;


public class TaskMarkingSketching : MonoBehaviour {

 
	public GameObject mark;
	public Transform taskMarkPosition;

	public static float Xvalue;
	public static float Yvalue;
	public static float Zvalue;
	
	// new
	public static float SkthX;
	public static float SkthY;
	public static float SkthZ;
     // 

	public Transform Righthand;
	public Transform PointingMark;
	public GameObject SketchingLine;
	//TrailRenderer line;

	public float time = 999999999999999999999f;
 
	void Update () {
		
		//TrailRenderer  SketchingLiner = SketchingLine.GetComponent(typeof(TrailRenderer )) as TrailRenderer ;


		Ray ray = new Ray (Righthand.transform.position, Righthand.transform.forward);

		RaycastHit hit;

 
				if (Physics.Raycast (ray, out hit)) {



				PointingMark.transform.position = new Vector3(hit.point.x,hit.point.y,hit.point.z);
			if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)) 
			{
 
				var clone=	Instantiate(mark, hit.point, Quaternion.identity);

 				Xvalue = hit.point.x;
				Yvalue = hit.point.y;
				Zvalue = hit.point.z;
				Vector3	taskMarkPosition = new Vector3(Xvalue,Yvalue,Zvalue);


  				Debug.Log ("The Marked Task Position at: " + taskMarkPosition);

 
				this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteValuesToSend, Constants.cSendMessagesMode, Xvalue, Yvalue, Zvalue);


			}

			if (OVRInput.Get(OVRInput.RawButton.RHandTrigger)) {


				// create a sketch
				// line = SketchingLine.GetComponent(typeof(TrailRenderer)) as TrailRenderer;
				//line.startWidth = 0.5f;
				//line.endWidth = 0.5f;

				//ine.startWidth = 0.01f;qq		
				//line.startWidth = 0.01f;
					//line.endWidth = 0.01f;
			//	line.material.color = Color.red;
				//line.time = 9999999999999999999999999f;
				 

			//	SketchingLiner.time = 999999999999999999999f;

 				SketchingLine.SetActive(true);
				
				// new
					SkthX=	SketchingLine.transform.position.x;
					SkthY=	SketchingLine.transform.position.y;
					SkthZ=	SketchingLine.transform.position.z;
                // 


				Debug.Log ("Sketching in process!!!!!");
				
				// pass the values for the remote sketch
				
				this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteSketchValuesToSend, Constants.cSendMessagesMode, SkthX, SkthY, SkthZ); // new

			}

			if (OVRInput.GetUp(OVRInput.RawButton.RHandTrigger)) {

			//	SketchingLiner.time = 0.0f;
				//TrailRenderer line = SketchingLine.GetComponent(typeof(TrailRenderer)) as TrailRenderer;

				//line = null;
				//LineRenderer lineRenderer = SketchingLine.GetComponent<LineRenderer>();
 				//lineRenderer.SetPosition(0, Vector3.zero);

				SketchingLine.SetActive(false);

               // SketchingLine = null; // new

				Debug.Log ("Sketching in stopped!!!!!");

			}


		} 

		if(Input.GetKey(KeyCode.Space))
		{
	 
		}

	
	}

	//Draw a line from start to touched position
	/*private void drawToMouse(Vector3 start) {
		if(line)  Destroy(line);
		line = Instantiate (linePrototype) as LineRenderer;
		Vector3 pointB = inputCamera.ScreenToWorldPoint (Input.mousePosition);
		start = inputCamera.ScreenToWorldPoint (start);
		line.SetVertexCount (2);
		line.SetPosition (0, start);
		line.SetPosition (1, pointB);
	}
	*/

	[RPC]
	public void RPCRemoteValuesToSend(float Xvalue, float Yvalue, float Zvalue)
	{
 
	}

	[RPC]
	public void RPCRemoteSketchValuesToSend(float SkthX, float SkthY,float SkthZ)  // new
	{

	}
}

