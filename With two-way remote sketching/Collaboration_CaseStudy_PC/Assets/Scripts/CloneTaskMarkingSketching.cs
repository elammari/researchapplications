﻿using UnityEngine;
using System.Collections;
using System;


public class CloneTaskMarkingSketching : MonoBehaviour {


	public GameObject markObject;
	public float offset =1.0f;
	private GameObject markObjectInstance;
	//public Toggle ResetToggle;

	public GameObject mark;
	public Transform taskMarkPosition;

	public static float Xvalue;
	public static float Yvalue;
	public static float Zvalue;

	// new
	public static float SkthX;
	public static float SkthY;
	public static float SkthZ;

	public Transform Righthand;
	public Transform PointingMark;
	public GameObject SketchingLine;


	void Update () {

		//TrailRenderer  SketchingLiner = SketchingLine.GetComponent(typeof(TrailRenderer )) as TrailRenderer ;


		Ray ray = new Ray (Righthand.transform.position, Righthand.transform.forward);

		RaycastHit hit;


		if (Physics.Raycast (ray, out hit)) {



			PointingMark.transform.position = new Vector3(hit.point.x,hit.point.y,hit.point.z);
			if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)) 
			{

				var clone=	Instantiate(mark, hit.point, Quaternion.identity);

				Xvalue = hit.point.x;
				Yvalue = hit.point.y;
				Zvalue = hit.point.z;
				Vector3	taskMarkPosition = new Vector3(Xvalue,Yvalue,Zvalue);


				Debug.Log ("The Marked Task Position at: " + taskMarkPosition);


				this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteValuesToSend, Constants.cSendMessagesMode, Xvalue, Yvalue, Zvalue);


			}

			if (OVRInput.Get(OVRInput.RawButton.RHandTrigger)) {

				// Cast a ray and find where the Bulding element (e.g., wall)
				//Ray ray = Camera.main.ViewportPointToRay (Input.mousePosition));
				//	Ray ray = Camera.main.ScreenPointToRay (new Vector3(0.5f,0.5f,0.0f)); 
				//Ray ray = new Ray (Righthand.transform.position, Righthand.transform.forward);

				//RaycastHit info;


				if (Physics.Raycast(ray, out hit)) 

				{

					// Instantiate line renderer object

					markObjectInstance = Instantiate(markObject,hit.point, Quaternion.identity) as GameObject;

					//	lineObjectInstance = Instantiate(lineObject,info.point*-offset,Quaternion.identity) as TrailRenderer;

					//		this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteSketchValuesToSend, Constants.cSendMessagesMode, Xvalue, Yvalue, Zvalue);


				}

				// Check to see if we have a line object instance
				if(markObjectInstance)
				{

					//	Ray ray = Camera.main.ViewportPointToRay(Input.mousePosition));
					//Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); 

				//	RaycastHit info;

					if (Physics.Raycast(ray, out hit))

					{

						// move our instantiated line renderer
						markObjectInstance.transform.position=hit.point;

						//	this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteSketchValuesToSend, Constants.cSendMessagesMode, Xvalue, Yvalue, Zvalue);

					}

				}

				// new
				SkthX=	markObjectInstance.transform.position.x;
				SkthY=	markObjectInstance.transform.position.y;
				SkthZ=	markObjectInstance.transform.position.z;
				// 


				Debug.Log ("Sketching in process!!!!!");

				// pass the values for the remote sketch

				this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteSketchValuesToSend, Constants.cSendMessagesMode, SkthX, SkthY, SkthZ); // new

			}

			if (OVRInput.GetUp(OVRInput.RawButton.RHandTrigger)) {

				markObjectInstance = null;

			}


		} 

		if(Input.GetKey(KeyCode.Space))
		{

		}


	}

	//Draw a line from start to touched position
	/*private void drawToMouse(Vector3 start) {
		if(line)  Destroy(line);
		line = Instantiate (linePrototype) as LineRenderer;
		Vector3 pointB = inputCamera.ScreenToWorldPoint (Input.mousePosition);
		start = inputCamera.ScreenToWorldPoint (start);
		line.SetVertexCount (2);
		line.SetPosition (0, start);
		line.SetPosition (1, pointB);
	}
	*/

	[RPC]
	public void RPCRemoteValuesToSend(float Xvalue, float Yvalue, float Zvalue)
	{

	}

	[RPC]
	public void RPCRemoteSketchValuesToSend(float SkthX, float SkthY,float SkthZ)  // new
	{

	}
}

