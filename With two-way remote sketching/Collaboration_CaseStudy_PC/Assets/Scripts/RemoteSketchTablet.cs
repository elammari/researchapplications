﻿
	using UnityEngine;
	using System.Collections;
	using UnityEngine.UI;
	using System;



	public class RemoteSketchTablet : MonoBehaviour 
	{


	public GameObject mark;
	public Transform taskMarkPosition;

	public static float Xvalue;
	public static float Yvalue;
	public static float Zvalue;

	// new
	public static float SkthXTblt;
	public static float SkthYTblt;
	public static float SkthZTblt;
	// 

	//public Transform Righthand;
	public Transform PointingMark;
	public GameObject SketchingLine;

	public float time = 999999999999999999999f;

	void Update () {

		//TrailRenderer  SketchingLiner = SketchingLine.GetComponent(typeof(TrailRenderer )) as TrailRenderer ;


		Ray ray = Camera.main.ScreenPointToRay (new Vector3(Input.mousePosition.x,Input.mousePosition.y,Input.mousePosition.z)); 

		RaycastHit hit;


		if (Physics.Raycast (ray, out hit)) {



			PointingMark.transform.position = new Vector3(hit.point.x,hit.point.y,hit.point.z);
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) 
			{
				/*

				var clone=	Instantiate(mark, hit.point, Quaternion.identity);

				Xvalue = hit.point.x;
				Yvalue = hit.point.y;
				Zvalue = hit.point.z;
				Vector3	taskMarkPosition = new Vector3(Xvalue,Yvalue,Zvalue);


				Debug.Log ("The Marked Task Position at: " + taskMarkPosition);


				this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteSketchTbltValuesToSend, Constants.cSendMessagesMode, Xvalue, Yvalue, Zvalue);

				*/
			}

			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) 
			{

				//	SketchingLiner.time = 999999999999999999999f;


				SketchingLine.SetActive(true);

				// new
				SkthXTblt=	SketchingLine.transform.position.x;
				SkthYTblt=	SketchingLine.transform.position.y;
				SkthZTblt=	SketchingLine.transform.position.z;
				// 


				Debug.Log ("Sketching in process!!!!!");

				// pass the values for the remote sketch

				this.GetComponent<NetworkView>().RPC(Constants.cRPCRemoteSketchTbltValuesToSend, Constants.cSendMessagesMode, SkthXTblt, SkthYTblt, SkthZTblt); // new

			}

			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended) 
			{

				//	SketchingLiner.time = 0.0f;


				SketchingLine.SetActive(false);

				// SketchingLine = null; // new

				Debug.Log ("Sketching in stopped!!!!!");

			}


		} 

		if(Input.GetKey(KeyCode.Space))
		{

		}


	}

	[RPC]
public void RPCRemoteSketchTbltValuesToSend(float SkthXTblt, float SkthYTblt, float SkthZTblt)
	{
	
	}
	}


