﻿using UnityEngine;
using System.Collections;
using System;


public class FingerMarking : MonoBehaviour {
	public Transform Righthand;
	public Transform Mark;
	public Transform PointingMark;
	//private GameObject markObjectInstance;
	// public GameObject markObject;
	public float offset =1.0f;
	public GameObject SketchingLine;
	// private GameObject markObjectInstance;

	void Start(){
 
	}


	void Update(){

		RaycastHit hit;
		Ray ray = new Ray (Righthand.transform.position, Righthand.transform.forward);





		if (Physics.Raycast (ray, out hit))
			Debug.DrawLine (Righthand.transform.position,hit.point, Color.magenta);

			PointingMark.transform.position = new Vector3(hit.point.x,hit.point.y,hit.point.z);


 
 
		if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)) {

 			Instantiate (Mark, hit.point, Quaternion.identity);
		 
		}

		if (OVRInput.Get(OVRInput.RawButton.RHandTrigger)) {
 
			//markObjectInstance = Instantiate(markObject,hit.point+hit.normal*offset, Quaternion.identity) as GameObject;

			//markObjectInstance.transform.position=new Vector3(hit.point.x,hit.point.y,hit.point.z);




			SketchingLine.SetActive(true);

			Debug.Log ("Sketching in process!!!!!");

		}

		if (OVRInput.GetUp(OVRInput.RawButton.RHandTrigger)) {

		 	SketchingLine.SetActive(false);

		//	markObjectInstance = null;


			Debug.Log ("Sketching in stopped!!!!!");

		}
	}


	}
